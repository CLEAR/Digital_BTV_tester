#---------------------------DATA ANALYSIS----------------------------------------------------#
# This script assess performances of the newly commissioned Basler 620 imaging system and    #
# uses it for emittance and Twiss parameters measurements, comparing the results with the    #
# ones obtained with the standard BTV system                                                 #                      #

import numpy as np
import matplotlib
matplotlib.use('Qt5Agg')
from math import floor
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.signal import wiener
from scipy import stats
from matplotlib import rcParams
import h5py
import time
import extract_data


fileData='New_QuadScan510_8_x_plane_good_4rd_qScan_CA.QFD0510' #Data file path

### CUADRUPOLE SCAN ###
DRIFT_Camera= 1.656
def data_quad(qnumber):
    if qnumber == 350 or qnumber == 355 or qnumber == 360:
        q1 = 'CA.QFD0360'
        q2 = 'CA.QDD0355'
        q3 ='CA.QFD0350'
        DRIFT1= 1.127
        DRIFT2= 0.224
        DRIFT3= 0.224

    elif qnumber == 510 or qnumber == 515 or qnumber==520:
        q1 = 'CA.QFD0520'
        q2 = 'CA.QFD0515'
        q3 ='CA.QFD0510'
        DRIFT1= 2.57
        DRIFT2= 0.274
        DRIFT3= 0.274
    return q1, q2, q3, DRIFT1, DRIFT2, DRIFT3


#......GLOBAL SETUP INFORMATION............

qnumber= 510
q1, q2, q3, DRIFT1, DRIFT2, DRIFT3 = data_quad(qnumber= 510)
qname = "CA.QFD0"+str(qnumber)

## CURRENT RANGE ##
currents_start = 21
currents_end   = 25
currents_step   = 1.0
currents = np.arange(currents_start,currents_end+currents_step*0.5,currents_step)

#...... CAMERAS .......
#New camera
camera_new= "CA.BTV0620-DIG"
camProperty_new = "Acquisition"
camField_new = "image1"
#Old camera
camera= "CA.BTV0620"
camProperty = "Image"
camField = "imageSet"

#Constant VARIABLES
energy= 200.0
light_speed = 299792458
electron_rest_en = 0.5109989461
ECQD = 0.056
QUAD_L = 0.226
max_sigx=1.5*1e-3

#CALIBRATION FACTORS
if camera== "CA.BTV0390" :
    coeff_x = 8/124.0
    coeff_y = 8*np.cos(15*np.pi/180)/223.0
elif camera == "CA.BTV0620":
    coeff_x = 4/126
    coeff_y = 4/117
else:
    print('No valid camera')

if camera_new == "CA.BTV0620-DIG":
    coeff_x_new = 1/41
    coeff_y_new = 4/166
else:
    print('No second camera')

#####################################################################################################
#.....FIT DATA FUNCTIONS                                                                     #
#...  This set of functions takes the beam images and perform gaussian fittings of the beam         #
#.... in both planes,  horizontal and vertical of both cameras BTV and Basler to return             #
#...  the beam sizes mean  and the standard error of the mean per each current                      #

def _calc_moments(axis,proj):
    dx=axis[1]-axis[0]
    Norm= np.trapz(proj,dx=dx)
    cen= np.trapz(proj*axis, dx=dx)/Norm
    sec= np.trapz(proj*(axis-cen)**2, dx=dx)/Norm
    std=np.sqrt(sec)
    return cen, std

def _gaussian(x,amp,mu,  sigma, y0):
    return amp*np.exp(-(x-mu)**2.0/(2.0*sigma**2.0))+y0

def _fit_gaussian(x,y,amp=None,mu=None,  sigma=None, y0=None):
    amp=amp or np.amax(y)
    par=_calc_moments(x,y)
    mu=mu or par[0]
    sigma=sigma or par[1]
    y0 = y0 or np.mean(y)
    try:
        p_opt, pcov = curve_fit(_gaussian, x,y , (amp, mu, sigma, y0))
    except Exception:
        p_opt=(amp, mu, sigma, y0)
        print("Fitting Problem")
    return p_opt

def proj(matrix):
    proj_x= np.sum(matrix, axis=0)
    proj_y=np.sum(matrix, axis=1)
    return proj_x, proj_y

def calc_roi1(image):
    proj_x = image.sum(axis=0)
    proj_y = image.sum(axis=1)
    axis_x = np.arange(image.shape[1])
    axis_y = np.arange(image.shape[0])
    mux, sigmax= _calc_moments(axis_x, proj_x)
    muy, sigmay = _calc_moments(axis_y, proj_y)
    n_sigma=0.5
    roi_size_x = n_sigma*sigmax/np.sqrt(2)
    roi_size_y =  n_sigma*sigmay/np.sqrt(2)
    strt_x, end_x = np.array([-1, 1])*roi_size_x + int(mux)
    strt_y, end_y = np.array([-1, 1])*roi_size_y + int(muy)
    strt_x =floor( max(strt_x, 0))
    strt_y =floor( max(strt_y, 0))
    end_x = floor(min(end_x, image.shape[1]))
    end_y = floor(min(end_y, image.shape[0]))

    image = image[strt_y:end_y, strt_x:end_x]
    proj_x = image.sum(axis=0)
    proj_y = image.sum(axis=1)
    axis_x = axis_x[strt_x:end_x]
    axis_y = axis_y[strt_y:end_y]
    return proj_x, proj_y, axis_x, axis_y

def calc_roi(newValue):
    proj_x, proj_y = proj(newValue)
    x=list(range(len(proj_x)))
    y=list(range(len(proj_y)))
    #proj_x, proj_y = proj_x[20:len(proj_x)-20], proj_y[20:len(proj_y)-20]
    max_x=np.where(proj_x == max(proj_x))[0]
    max_y=np.where(proj_y == max(proj_y))[0]
    mux, sigmax= _calc_moments(x, proj_x)
    muy, sigmay = _calc_moments(y, proj_y)
    n_sigma=1.5#.5
    roi_size_x = floor(n_sigma*sigmax/np.sqrt(2))
    roi_size_y =  floor(n_sigma*sigmay/np.sqrt(2))
    strt_x= max_x[0]-roi_size_x
    strt_y=max_y[0]-roi_size_y
    end_x=max_x[0]+roi_size_x
    end_y=max_y[0]+roi_size_y
    strt_x = max(strt_x, 0)
    strt_y = max(strt_y, 0)
    end_x = min(end_x, newValue.shape[1])
    end_y = min(end_y, newValue.shape[0])
    proj_x=proj_x[strt_x:end_x]
    proj_y=proj_y[strt_y:end_y]
    x=list(range(len(proj_x)))
    y=list(range(len(proj_y)))
    return proj_x, proj_y, x, y

def _3std(arry):
    Data=np.array(arry)
    N=np.abs(Data-Data.mean())<=(2*Data.std())
    NewData=[]
    for i in range(len(N)):
        if N[i]==True: # and Data[i]>max_sigx:
            NewData.append(Data[i])
    return NewData

def perf_ana(ima, coeff_x, coeff_y ):
    proj_x, proj_y, x, y = calc_roi(ima)
    amp_x, cen_x, std_x, off_x= _fit_gaussian(x, proj_x)
    amp_y, cen_y, std_y, off_y= _fit_gaussian(y, proj_y)
    cen_x = cen_x
    cen_y = cen_y
    sigma_x = abs(std_x)*0.001*coeff_x
    sigma_y = abs(std_y) *0.001*coeff_y
    return sigma_x, sigma_y

def save_to_file(fname, I1, I2, I3, s_x, s_y, s_x_new, s_y_new):
    header = 'Plane = {0:s}\n'.format('x')
    header += '{0:15s} {1:15s} {2:15} {3:15} {4:15},{5:15}\n'.format('','','','Old Camera','', 'New Camera')
    header += '{0:15s} {1:15s} {2:15s} {3:15s}{4:15s} {5:15s}{6:15s}'.format('[A] '+q1, '[A] '+q2,'[A] '+q3,'Beam Size x [m]','Beam Size y [m]' , 'Beam Size x [m]', 'Beam Size y [m]')
    np.savetxt(fname, np.column_stack((I1, I2, I3, s_x, s_y, s_x_new, s_y_new)), header=header, fmt='%-15.9f')

def extrac_sigma_error(filename, parameter, currents, quad1, quad2, quad3, coeff_x,  coeff_y):
    result_sx=[]
    result_sy=[]
    result_c1=[]
    result_c2=[]
    result_c3=[]
    spread_x=[]
    spread_y=[]
    for current in currents:
        sigm_x=[]
        sigm_y=[]
        hf = h5py.File(filename+'_{}.h5'.format(current), 'r')
        try:
            current1 = hf.attrs['I_'+quad1]
        except Exception:
            current1 = 0.000
        try:
            current2 = hf.attrs['I_'+quad2]
        except Exception:
            current2 = 0.000
        try:
            current3 = hf.attrs['I_'+quad3]
        except Exception:
            current3 = 0.000
        for dataCounter in range(10):
            newValue=hf[parameter%(dataCounter,)][:]
            s_x, s_y = perf_ana(newValue, coeff_x,  coeff_y)
            sigm_x.append(s_x)
            sigm_y.append(s_y)
        hf.close()

        sigm_x= sorted(_3std(sigm_x))[1:-1]
        sigm_y=sorted(_3std(sigm_y))[1:-1]

        spread_x.append(stats.sem(sigm_x))
        spread_y.append(stats.sem(sigm_y))

        result_sx.append(np.mean(sigm_x))
        result_sy.append(np.mean(sigm_y))

        result_c1.append(current1)
        result_c2.append(current2)
        result_c3.append(current3)
    return np.array([np.array(result_sx), np.array(spread_x),
                    np.array(result_sy), np.array(spread_y),
                    np.array(result_c1), np.array(result_c2), np.array(result_c3)])

#####################################################################################################
#     Twiss Parameters and Emittance Calculation                                                    #
#...  This set of functions uses the beam mean sizes obtained after the fittings  and the current   #
#...  of each quadrupole of the triplet. Using the currents, its possible to calculate the transfer #
#...  matrix between S_0 and S_1. Then, the it calculates the pseudo-inverse of the transfer        #
#...  matrix and it multiples with the beam sizes obtained in the fitting.                          #
# ... After the multiplication, the components of the sigma matrix  at S_0 are obtained.            #
#...  With this values is easily to calculate Twiss parameters  and normalized emittance            #

def get_K_from_I(I, E):
    kin_en = np.sqrt(E*E - electron_rest_en*electron_rest_en)
    return I*ECQD*light_speed/kin_en/1e6

def _twiss(s_11 , s_12, s_22, E):
    emit0 = np.sqrt(abs(s_11 * s_22 - s_12 * s_12))
    beta0 = s_11 / emit0
    alpha0 = -s_12 / emit0
    gamma0 = s_22 / emit0
    nemit0 = emit0 * E / electron_rest_en * 1e6  # in mm.mrad
    return nemit0, beta0, alpha0, gamma0

def gettransmat(type, L, gamma, K=None, B=None):
    R = np.eye(6)

    if type.lower().startswith('qu') and abs(K) < 1e-10:
        type = 'drift'
    if type.lower().startswith('dr'):
        R = np.array([
            [1, L, 0, 0, 0, 0],
            [0, 1, 0, 0, 0, 0],
            [0, 0, 1, L, 0, 0],
            [0, 0, 0, 1, 0, 0],
            [0, 0, 0, 0, 1, L/gamma**2],
            [0, 0, 0, 0, 0, 1],
            ])
    elif type.lower().startswith('qu') and K is not None:
        kq = np.sqrt(abs(K))
        c = np.cos(kq*L)
        s = np.sin(kq*L)
        ch = np.cosh(kq*L)
        sh = np.sinh(kq*L)
        if K > 0:
            x11, x12, x21 = c,  1/kq*s, -kq*s
            y11, y12, y21 = ch, 1/kq*sh, kq*sh
        else:
            x11, x12, x21 = ch, 1/kq*sh, kq*sh
            y11, y12, y21 = c,  1/kq*s, -kq*s
        R = np.array([
            [x11, x12, 0,   0,   0, 0],
            [x21, x11, 0,   0,   0, 0],
            [0,   0,   y11, y12, 0, 0],
            [0,   0,   y21, y11, 0, 0],
            [0,   0,   0,   0,   1, L/gamma**2],
            [0,   0,   0,   0,   0, 1],
            ])
    return R

def _get_resp_mat(K1, K2, K3, energy, sigma):
    gamma = energy/electron_rest_en
    R = np.zeros((len(K1), 6, 6))
    Rd1 = gettransmat('drift', L=DRIFT1, gamma=gamma, K=0.0)
    Rd2 = gettransmat('drift', L=DRIFT2, gamma=gamma, K=0.0)
    Rd3 = gettransmat('drift', L=DRIFT3, gamma=gamma, K=0.0)
    for i in range(len(K1)):
        Rq1 = gettransmat('quad', L=QUAD_L, gamma=gamma, K = K1[i])
        Rq2= gettransmat('quad', L=QUAD_L, gamma=gamma, K = K2[i])
        Rq3= gettransmat('quad', L=QUAD_L, gamma=gamma, K = K3[i])
        R[i] = Rd1 @ Rq1 @ Rd2 @ Rq2 @ Rd3 @ Rq3
    R11 = R[:, 0, 0]
    R12 = R[:, 0, 1]
    R33 = R[:, 2, 2]
    R34 = R[:, 2, 3]
    Rx = np.column_stack((R11*R11, 2*R11*R12, R12*R12))
    Ry = np.column_stack((R33*R33, 2*R33*R34, R34*R34))
    return Rx, Ry

def _trans_matrix_method(I1, I2, I3, sigma, energy, pl):
    if q1 == 'CA.QFD0520':
        I1= -I1 if pl=='x' else I1
    I2= -I2 if pl=='x' else I2
    I3= -I3 if pl=='y' else I3
    K1 = get_K_from_I(I1, energy)
    K2 = get_K_from_I(I2, energy)
    K3 = get_K_from_I(I3, energy)
    Rx, Ry = _get_resp_mat(K1, K2, K3, energy, sigma)
    R = Rx if pl == 'x' else Ry
    pseudo_inv = (np.linalg.inv(np.transpose(R) @ R) @ np.transpose(R))
    [s_11, s_12, s_22] = pseudo_inv @ (sigma*sigma)
    nemitx, betax, alphax, gammax = _twiss(s_11, s_12, s_22, energy)
    return nemitx, betax, alphax, gammax


def _thin_lens_method(I1, I2, I3, sigma, energy, pl):
    I1= -I1 if pl=='x' else I1
    I2= -I2 if pl=='x' else I2
    I3= -I3 if pl=='y' else I3
    k1 = get_K_from_I(I1, energy)[0]
    k2 = get_K_from_I(I2, energy)[0]
    k3 = get_K_from_I(I3, energy)

    d1 = DRIFT1 + QUAD_L/2
    d2 = DRIFT2 + QUAD_L
    d3 = DRIFT3 + QUAD_L

    a, b, c = np.polyfit(k3, [n**2 for n in sigma], 2)
    L = QUAD_L
    # FIT IN K3
    s_11= a/(L*L*(d1+d2+d3-L*d1*d2*k1-L*d1*d3*k1-L*d1*d3*k2-L*d2*d3*k2+L*L*d1*d2*d3*k1*k2)**2)
    s_12 = (-b-2*L*(d2+d3+d1*(1-L*(d2+d3)*k1)+L*d3*(-d2+d1*(-1+L*d2*k1))*k2)*(1-L*d2*k2+L*d1*(-k1+(-1+L*d2*k1)*k2))*s_11)/(2*L*(d2+d3+d1*(1-L*(d2+d3)*k1)+L*d3*(-d2+d1*(-1+L*d2*k1))*k2)**2)
    s_22 = -(-c+(-1+L*d2*k2+L*d1*(k1+k2-L*d2*k1*k2))**2*s_11+2*(1-L*d1*k1+L*(-d2+d1*(-1+L*d2*k1))*k2)*(d2+d3+d1*(1-L*(d2+d3)*k1)+L*d3*(-d2+d1*(-1+L*d2*k1))*k2)*s_12)/((d2+d3+d1*(1-L*(d2+d3)*k1)+L*d3*(-d2+d1*(-1+L*d2*k1))*k2)**2)

    nemit, beta, alpha, gamma = _twiss(s_11, s_12, s_22, energy)
    return nemit, beta, alpha, gamma

##########################################################################################################
#     Monte-Carlo error propagation                                                                      #
#...  The idea behind the Monte-Carlo technique is to generate many possible solutions of the emittance  #
#...  and Twiss parameters, each time varying the input data (beam size) randomly  within their stated   #

def emit_montec(I1, I2, I3, sigmas, error, energy, plane ):
    sigma_montecarlo = []
    N = 10000
    for i in range(len(sigmas)):
        sigma_montecarlo.append(np.random.normal(sigmas[i], error[i], N))
    nemit_t=[]
    beta_t=[]
    alpha_t=[]
    gamma_t=[]
    sigma_montecarlo=np.array(sigma_montecarlo)
    for j in range(len(sigma_montecarlo[0])):
        sigmx = sigma_montecarlo[:,j]
        nemit, beta, alpha, gamma=_trans_matrix_method(I1, I2, I3, sigmx, energy, plane)
        nemit_t.append(nemit)
        beta_t.append(beta)
        alpha_t.append(alpha)
        gamma_t.append(gamma)
    return np.array([nemit_t, beta_t, alpha_t, gamma_t])


##########################################################################################################
#     Statistical Analysis                                                                               #
#...  This set of functions s tests the hypothesis that the estimated Twiss parameters for both cameras  #
#...  are actually correct. To compare the estimates from the two cameras is necessary to compare the    #
#...  two means. If the two distributions of the means are the same, the distribution of the difference  #
#...  would be roughly centered at zero                                                                  #

def gaussian(x, mu, sigma):
    norm = 1. / (sigma * np.sqrt(2. * np.pi))
    return norm * np.exp(-(x - mu) ** 2. / (2. * sigma ** 2))

def p_value(axis,proj):
    dx=axis[1]-axis[0]
    Norm= np.trapz(proj,dx=dx)
    pos= False
    i=0
    while pos is False:
        if axis[i]>= 0 or i>=len(axis)-1:
            pos = True
        i += 1
    p = np.trapz(proj[:i],dx=dx)/Norm
    return 1-p

def fit_histo(data, data1):
    if data.mean() >= data1.mean() :
        data_test= data1-data
    else:
        data_test = data-data1
    data_test_x = np.linspace(min(data_test), max(data_test), 1000)
    data_test_y = gaussian(data_test_x, data_test.mean(), data_test.std())
    pval= p_value(data_test_x, data_test_y)
    return data_test, data_test_x, data_test_y, pval


def create_curr(ini, fin):
    currents_step   = 1.0
    curren = np.arange(ini,fin+currents_step*0.5,currents_step)
    return curren

##########################################################################################################
# .....................    INLIZIALIZARTION         .....................................                #

set_current= np.array([create_curr(21,25), create_curr(20,26), create_curr(19,27),
                        create_curr(18,28),create_curr(17,29), create_curr(16,30),
                        create_curr(14,30), create_curr(12,30), create_curr(10,30)])
final_results=[]
final_results2=[]
results_p=[]
for currents in set_current:
    sigma2, error_x_new, sigma2y, error_y_new, I1, I2, I3 = extrac_sigma_error(fileData, "newCam_%05i", currents, q1, q2, q3, coeff_x_new,  coeff_y_new)
    sigma1, error_x, sigma1y, error_y, I1, I2, I3 = extrac_sigma_error(fileData, "oldCam_%05i", currents, q1, q2, q3, coeff_x,  coeff_y)
    New=False
    [nemit, beta, alpha, gamma] = emit_montec(I1, I2, I3, sigma1, error_x, energy, plane='x')
    New=True
    [nemit1, beta1, alpha1, gamma1] = emit_montec(I1, I2, I3, sigma2, error_x_new,  energy, plane='x')

    nemit_test, nemit_test_x, nemit_test_y, nemit_test_p = fit_histo(nemit, nemit1)
    beta_test, beta_test_x, beta_test_y, beta_test_p =  fit_histo(beta, beta1)
    alpha_test, alpha_test_x, alpha_test_y, alpha_test_p =  fit_histo(alpha, alpha1)
    gamma_test, gamma_test_x, gamma_test_y, gamma_test_p =  fit_histo(gamma, gamma1)
    print('Current range:')
    print('I1:'+str(int(I1[0]))+'-'+str(int(I1[-1]))+' A, I2:'+str(int(I2[0]))+'-'+str(int(I2[-1]))+' A, I3:'+str(int(I3[0]))+'-'+str(int(I3[-1]))+' A')
    print('old camera')
    print('Normazed emittance:', nemit.mean() ,'+/-', nemit.std())
    print('beta:',  beta.mean() ,'+/-', beta.std())
    print('alpha:', alpha.mean(),'+/-', alpha.std())
    print('gamma:' , gamma.mean() ,'+/-',  gamma.std())

    print('new camera')
    print('Normazed emittance:', nemit1.mean() ,'+/-', nemit1.std())
    print('beta:',  beta1.mean() ,'+/-', beta1.std())
    print('alpha:', alpha1.mean(),'+/-', alpha1.std())
    print('gamma:' , gamma1.mean() ,'+/-', gamma1.std())

    final_results.append([nemit,beta, alpha, gamma])
    final_results2.append([nemit1,beta1, alpha1, gamma1])
    results_p.append(nemit_test_p)


def plot_r():
    '''
    This function plots the global results
    '''
    K3 = get_K_from_I(I3, energy)
    a, b, c = np.polyfit(K3, sigma1*sigma1, 2)
    yd = np.sqrt(np.polyval([a, b, c], K3))

    plt.subplot2grid((4,4),(0,0), colspan=4, rowspan=2)
    plt.plot(I3,yd*1000, label = 'Old Camera')

    plt.xlabel('I [A]')
    plt.ylabel('beam size mm.')
    plt.errorbar(I3,sigma1*1000, yerr=error_x*1000, fmt='.b', color = 'blue')
    #plt.text(I3[2], sigma1[1]*1000, r'$\epsilon_N$ :'+str(round(nemit.mean(),3))+'+/-'+str(round(nemit.std(),3))+' [mm.mmrad] \n'+r' $\beta$:'+str(round(beta.mean(),3))+'+/-'+str(round(beta.std(),3))+' [m]\n'+r' $\alpha$:'+str(round(alpha.mean(),3))+'+/-'+str(round(alpha.std(),3)))
    #plt.title('Old Camera')

    a, b, c = np.polyfit(K3, sigma2*sigma2, 2)
    yd1 = np.sqrt(np.polyval([a, b, c], K3))
    #plt.subplot2grid((3,4),(0,2), colspan=2)
    plt.plot(I3,yd1*1000, label= 'New Camera')

    #plt.xlabel('I [A]')
    plt.errorbar(I3,sigma2*1000, yerr=error_x_new*1000, fmt='.r', color= 'orange')
    #plt.text(I3[2], sigma2[1]*1000, r'$\epsilon_N$ :'+str(round(nemit1.mean(),3))+'+/-'+str(round(nemit1.std(),3))+' [mm.mmrad] \n'+r' $\beta$:'+str(round(beta1.mean(),3))+'+/-'+str(round(beta1.std(),3))+' [m]\n'+r' $\alpha$:'+str(round(alpha1.mean(),3))+'+/-'+str(round(alpha1.std(),3)))
    #plt.title('New Camera')
    plt.legend(loc='upper right')

    plt.subplot2grid((4,4),(2,0))
    plt.hist(nemit, bins=50, range=[min(nemit), max(nemit)], normed=True)
    plt.hist(nemit1, bins=50, range=[min(nemit1), max(nemit1)], normed=True)
    #plt.hist(nemit_propa, bins=50, range=[min(nemit_propa), max(nemit_propa)], normed=True)

    plt.title('Emittance')

    plt.subplot2grid((4,4),(2,1))
    plt.hist(beta, bins=50, range=[min(beta), max(beta)], normed=True)
    plt.hist(beta1, bins=50, range=[min(beta1), max(beta1)], normed=True)

    #plt.hist(beta_p, bins=50, range=[min(beta_p), max(beta_p)], normed=True)
    #plt.text(26 ,4, 'p: '+str(round(beta_test_p,2)),{'color': 'C2', 'fontsize': 8}, va="top", ha="right")
    plt.title('Beta')

    plt.subplot2grid((4,4),(2,2))
    plt.hist(alpha, bins=50, range=[min(alpha), max(alpha)], normed=True)
    plt.hist(alpha1, bins=50, range=[min(alpha1), max(alpha1)], normed=True)
    #plt.hist(alpha_p, bins=50, range=[min(alpha_p), max(alpha_p)], normed=True)

    #plt.text(0.20 ,40,'p: '+str(round(alpha_test_p, 2)),{'color': 'C2', 'fontsize': 8}, va="top", ha="right")
    plt.title('Alpha')

    plt.subplot2grid((4,4),(2,3))
    plt.hist(gamma, bins=50, range=[min(gamma), max(gamma)], normed=True)
    plt.hist(gamma1, bins=50, range=[min(gamma1), max(gamma1)], normed=True)
    #plt.hist(gamma_p, bins=50, range=[min(gamma_p), max(gamma_p)], normed=True)

    #plt.text(0.05 ,1000,'p: '+str(round(gamma_test_p, 2)),{'color': 'C2', 'fontsize': 8}, va="top", ha="right")
    plt.title('Gamma')

    plt.subplot2grid((4,4),(3,0))
    plt.hist(nemit_test, bins=50, range=[min(nemit_test), max(nemit_test)], normed=True)
    plt.plot(nemit_test_x, nemit_test_y)
    plt.text(max(nemit_test_x) , max(nemit_test_y),'p: '+str(round(nemit_test_p, 4 )),{'fontsize': 8}, va="top", ha="right")

    #plt.title('Emittance p test')

    plt.subplot2grid((4,4),(3,1))
    plt.hist(beta_test, bins=50, range=[min(beta_test), max(beta_test)], normed=True)
    plt.plot(beta_test_x, beta_test_y)
    plt.text(max(beta_test_x) , max(beta_test_y), 'p: '+str(round(beta_test_p, 4)),{'fontsize': 8}, va="top", ha="right")
    #plt.title('Beta p test')

    plt.subplot2grid((4,4),(3,2))
    plt.hist(alpha_test, bins=50, range=[min(alpha_test), max(alpha_test)], normed=True)
    plt.plot(alpha_test_x, alpha_test_y)
    plt.text(max(alpha_test_x) , max(alpha_test_y),'p: '+str(round(alpha_test_p ,4)),{ 'fontsize': 8}, va="top", ha="right")
    #plt.title('alpha p test')

    plt.subplot2grid((4,4),(3,3))
    plt.hist(gamma_test, bins=50, range=[min(gamma_test), max(gamma_test)], normed=True)
    plt.plot(gamma_test_x, gamma_test_y)

    plt.text(max(gamma_test_x) , max(gamma_test_y),'p: '+str(round(gamma_test_p, 4)),{'fontsize': 8}, va="top", ha="right")
    #plt.title('Gamma p test')
    plt.show()
plot_r()


#############################################################################
#     Dynamic Range Analysis                                                #
#...  This block studies the results obtained for different dynamic ranges  #

def extract_d(arra, pos):
    arra= np.array(arra)
    valor_total= []
    valor_total_std= []
    for resul in arra[:,pos]:
        valor_total.append(resul.mean())
        valor_total_std.append(resul.std())
    return np.array(valor_total), np.array(valor_total_std)

emit_res_fin,  emit_err_fin= extract_d(final_results, 0)
emit_res_fin2,  emit_err_fin2= extract_d(final_results2, 0)


plt.subplot2grid((1,5),(0,0))
plt.suptitle('Dynamic Range', fontsize=16)
plt.errorbar([x+1 for x in range(len(emit_res_fin))],emit_res_fin, emit_err_fin, fmt='--o',  markersize=3, ecolor = 'darkblue',capsize=3,  linewidth=0.5, color='darkblue', label='BTV')
plt.errorbar([x+1 for x in range(len(emit_res_fin2))],emit_res_fin2, emit_err_fin2, fmt='--o',  markersize=3, ecolor = 'maroon',capsize=3,  linewidth=0.5, color='maroon', label='Basler')

plt.ylabel(r'$\epsilon$ [mm.mrad]', y=1, ha='right',size= 10 )
#plt.xlabel('Dynamic range', x=1, ha='right',size= 10)
plt.legend(loc='lower right')
#plt.show()
plt.subplot2grid((1,5),(0,1))
beta_res_fin,  beta_err_fin= extract_d(final_results, 1)
beta_res_fin2,  beta_err_fin2= extract_d(final_results2, 1)

plt.errorbar([x+1 for x in range(len(beta_res_fin))],beta_res_fin, beta_err_fin, fmt='--o',  markersize=3, ecolor = 'darkblue',capsize=3,  linewidth=0.5, color='darkblue', label='BTV')
plt.errorbar([x+1 for x in range(len(beta_res_fin2))],beta_res_fin2, beta_err_fin2, fmt='--o',  markersize=3, ecolor = 'maroon',capsize=3,  linewidth=0.5, color='maroon', label='Basler')
plt.ylabel(r'$\beta$ [m]', y=1, ha='right',size= 10 )
#plt.xlabel('Dynamic range', x=1, ha='right',size= 10)
#plt.legend(loc='lower right')
#plt.show()
plt.subplot2grid((1,5),(0,2))
alpha_res_fin,  alpha_err_fin= extract_d(final_results, 2)
alpha_res_fin2,  alpha_err_fin2= extract_d(final_results2, 2)

plt.errorbar([x+1 for x in range(len(alpha_res_fin))],alpha_res_fin, alpha_err_fin, fmt='--o',  markersize=3, ecolor = 'darkblue',capsize=3,  linewidth=0.5, color='darkblue', label='BTV')
plt.errorbar([x+1 for x in range(len(alpha_res_fin2))],alpha_res_fin2, alpha_err_fin2, fmt='--o',  markersize=3, ecolor = 'maroon',capsize=3,  linewidth=0.5, color='maroon', label='Basler')
#plt.plot(extract_d(final_results, 0))
#plt.plot(extract_d(final_results2, 0))
plt.ylabel(r'$\alpha$ ', y=1, ha='right',size= 10 )
#plt.xlabel('Dynamic range', x=1, ha='right',size= 10)
#plt.legend(loc='lower right')
#plt.show()
plt.subplot2grid((1,5),(0,3))
gamma_res_fin,  gamma_err_fin= extract_d(final_results, 3)
gamma_res_fin2,  gamma_err_fin2= extract_d(final_results2, 3)

plt.errorbar([x+1 for x in range(len(gamma_res_fin))],gamma_res_fin, gamma_err_fin, fmt='--o',  markersize=3, ecolor = 'darkblue',capsize=3,  linewidth=0.5, color='darkblue', label='BTV')
plt.errorbar([x+1 for x in range(len(gamma_res_fin2))],gamma_res_fin2, gamma_err_fin2, fmt='--o',  markersize=3, ecolor = 'maroon',capsize=3,  linewidth=0.5, color='maroon', label='Basler')
#plt.plot(extract_d(final_results, 0))
#plt.plot(extract_d(final_results2, 0))
plt.ylabel(r'$\gamma$ [1/m]', y=1, ha='right',size= 10 )
#plt.xlabel('Dynamic range', x=1, ha='right',size= 10)
#plt.legend(loc='lower right')
#plt.show()

plt.subplot2grid((1,5),(0,4))
plt.plot([x+1 for x in range(len(gamma_res_fin2))], results_p, linewidth=0.5,color='black',ls='-', marker='.', mec='red')
plt.ylabel('p value', y=1, ha='right',size= 10 )
plt.xlabel('Dynamic range', x=1, ha='right',size= 10)
plt.show()

print('Old Results:')
print('Emittance:' ,emit_res_fin.mean(),'$\pm$', emit_res_fin.std())
print('Beta:' ,beta_res_fin.mean(),'$\pm$', beta_res_fin.std())
print('Alpha:' ,alpha_res_fin.mean(),'$\pm$', alpha_res_fin.std())
print('Beta:' ,gamma_res_fin.mean(),'$\pm$', gamma_res_fin.std())

print('New Results:')
print('Emittance:' ,emit_res_fin2.mean(),'$\pm$', emit_res_fin2.std())
print('Beta:' ,beta_res_fin2.mean(),'$\pm$', beta_res_fin2.std())
print('Alpha:' ,alpha_res_fin2.mean(),'$\pm$', alpha_res_fin2.std())
print('Beta:' ,gamma_res_fin2.mean(),'$\pm$', gamma_res_fin2.std())


rcParams['font.family'] = 'sans-serif'

#############################################################
#     Specific Range Analysis                               #
#...  This block study a specific dynamic range result     #

#..............FIT PLOT............
K3 = get_K_from_I(I3, energy)
a, b, c = np.polyfit(K3, sigma1*sigma1, 2)
yd = np.sqrt(np.polyval([a, b, c], K3))
plt.plot(I3,yd*1000, label = 'Old Camera', color='darkblue',linewidth=.5)
plt.xlabel('I [A]', x=1, ha='right',size= 10 )
plt.ylabel('beam size [mm]', y=1, ha='right',size= 10)
#plt.fill_between(I3,sigma1*1000+error_x*1000, sigma1*1000-error_x*1000, color='black')
plt.errorbar(I3,sigma1*1000, yerr=error_x*1000, fmt='ob',  markersize=3, ecolor = 'black',capsize=3)
a, b, c = np.polyfit(K3, sigma2*sigma2, 2)
yd1 = np.sqrt(np.polyval([a, b, c], K3))
plt.plot(I3,yd1*1000, label= 'New Camera', color='maroon', linewidth=.5)
plt.errorbar(I3,sigma2*1000, yerr= error_x_new*1000, fmt='or', markersize=3, ecolor= 'maroon',capsize=3)
plt.legend(loc='best')
plt.show()

#......EMITTANCE.........................

fqy, fqx, ns= plt.hist(-nemit_test, bins=50, range=[min(-nemit_test), max(-nemit_test)], normed=True,fc='none', histtype='step', color='black', label='$\sigma_A-\sigma_B$', linewidth=.8)
plt.plot(-nemit_test_x, nemit_test_y, color='maroon', linewidth=.8, label='Fitting')
plt.text(min(-nemit_test_x) , max(fqy),'mean: '+str(round(-nemit_test_x.mean(),4))+'\n $\sigma$: '+str(round(nemit_test_x.std(),4))+'\n p: '+str(round(nemit_test_p, 4 )),{'fontsize': 10, 'weight' : 'bold'}, va="top", ha="left", bbox={'facecolor': 'white', 'alpha': 0.5, 'pad': 10})
plt.xticks([0],[0],size=12)
plt.yticks(size=10)
plt.legend(loc='upper right')
plt.fill_between(-nemit_test_x, nemit_test_y, where=-nemit_test_x<= 0,facecolor='maroon')
plt.xlabel('[mm.mrad]', x=1, ha='right',size= 10 )
plt.ylabel('amplitude', y=1, ha='right',size= 10)
plt.show()

qx, by, n = plt.hist(nemit, bins=50, range=[min(nemit), max(nemit)], normed=True, fc='none', histtype='step', color='black', label='BTV', linewidth=.8)
plt.text(max(by) , max(qx),'mean: '+str(round(nemit.mean(),4))+'\n $\sigma$: '+str(round(nemit.std(),4)), va="top", ha="right", bbox={'facecolor': 'white', 'alpha': 0.5, 'pad': 10},color='black', fontdict={'weight' : 'bold', 'fontsize': 10})
qx1, by1, n1 = plt.hist(nemit1, bins=50, range=[min(nemit1), max(nemit1)], normed=True, fc='none', histtype='step', color = 'maroon', label='Basler', linewidth=.8)
plt.text(min(by1) , max(qx),'mean: '+str(round(nemit1.mean(),4))+'\n $\sigma$: '+str(round(nemit1.std(),4)) , va="top", ha="left", bbox={'facecolor': 'white', 'alpha': 0.5, 'pad': 10}, color='maroon', fontdict={'fontsize': 10,'weight' : 'bold'})
plt.legend(loc='lower right')
plt.xlabel(r'$\epsilon$ [mmm.rad]', x=1, ha='right',size= 10 )
plt.ylabel('amplitude', y=1, ha='right',size= 10)
plt.show()

#...................BETA.....................
fqy, fqx, ns= plt.hist(-beta_test, bins=50, range=[min(-beta_test), max(-beta_test)], normed=True,fc='none', histtype='step', color='black', label='$\sigma_A-\sigma_B$', linewidth=.8)
plt.plot(-beta_test_x, beta_test_y, color='maroon', linewidth=.8, label='Fitting')
plt.text(min(-beta_test_x) , max(fqy),'mean: '+str(round(-beta_test_x.mean(),4))+'\n $\sigma$: '+str(round(beta_test_x.std(),4))+'\n p: '+str(round(beta_test_p, 4 )),{'fontsize': 10, 'weight' : 'bold'}, va="top", ha="left", bbox={'facecolor': 'white', 'alpha': 0.5, 'pad': 10})
plt.xticks([0],[0],size=12)
plt.yticks(size=10)
plt.legend(loc='upper right')
plt.fill_between(-beta_test_x, beta_test_y, where=-beta_test_x<= 0,facecolor='maroon')
plt.xlabel('[m]', x=1, ha='right',size= 10 )
plt.ylabel('amplitude', y=1, ha='right',size= 10)
plt.show()

qx, by, n = plt.hist(beta, bins=50, range=[min(beta), max(beta)], normed=True, fc='none', histtype='step', color='black', label='BTV', linewidth=.8)
plt.text(max(by) , max(qx),'mean: '+str(round(beta.mean(),4))+'\n $\sigma$: '+str(round(beta.std(),4)), va="top", ha="right", bbox={'facecolor': 'white', 'alpha': 0.5, 'pad': 10},color='black', fontdict={'weight' : 'bold', 'fontsize': 10})
qx1, by1, n1 = plt.hist(beta1, bins=50, range=[min(beta1), max(beta1)], normed=True, fc='none', histtype='step', color = 'maroon', label='Basler', linewidth=.8)
plt.text(min(by1) , max(qx),'mean: '+str(round(beta1.mean(),4))+'\n $\sigma$: '+str(round(beta1.std(),4)) , va="top", ha="left", bbox={'facecolor': 'white', 'alpha': 0.5, 'pad': 10}, color='maroon', fontdict={'fontsize': 10,'weight' : 'bold'})
plt.legend(loc='lower right')
plt.xlabel(r'$\beta$ [m]', x=1, ha='right',size= 10 )
plt.ylabel('amplitude', y=1, ha='right',size= 10)
plt.show()

#.........................ALPHA..........................

fqy, fqx, ns= plt.hist(-alpha_test, bins=50, range=[min(-alpha_test), max(-alpha_test)], normed=True,fc='none', histtype='step', color='black', label='$\sigma_A-\sigma_B$', linewidth=.8)
plt.plot(-alpha_test_x, alpha_test_y, color='maroon', linewidth=.8, label='Fitting')
plt.text(min(-alpha_test_x) , max(fqy),'mean: '+str(round(-alpha_test_x.mean(),4))+'\n $\sigma$: '+str(round(alpha_test_x.std(),4))+'\n p: '+str(round(alpha_test_p, 4 )),{'fontsize': 10, 'weight' : 'bold'}, va="top", ha="left", bbox={'facecolor': 'white', 'alpha': 0.5, 'pad': 10})
plt.xticks([0],[0],size=12)
plt.yticks(size=10)
plt.legend(loc='upper right')
plt.fill_between(-alpha_test_x, alpha_test_y, where=-alpha_test_x<= 0,facecolor='maroon')

plt.ylabel('amplitude', y=1, ha='right',size= 10)
plt.show()

qx, by, n = plt.hist(alpha, bins=50, range=[min(alpha), max(alpha)], normed=True, fc='none', histtype='step', color='black', label='BTV', linewidth=.8)
plt.text(max(by) , max(qx),'mean: '+str(round(alpha.mean(),4))+'\n $\sigma$: '+str(round(alpha.std(),4)), va="top", ha="right", bbox={'facecolor': 'white', 'alpha': 0.5, 'pad': 10},color='black', fontdict={'weight' : 'bold', 'fontsize': 10})
qx1, by1, n1 = plt.hist(alpha1, bins=50, range=[min(alpha1), max(alpha1)], normed=True, fc='none', histtype='step', color = 'maroon', label='Basler', linewidth=.8)
plt.text(min(by1) , max(qx),'mean: '+str(round(alpha1.mean(),4))+'\n $\sigma$: '+str(round(alpha1.std(),4)) , va="top", ha="left", bbox={'facecolor': 'white', 'alpha': 0.5, 'pad': 10}, color='maroon', fontdict={'fontsize': 10,'weight' : 'bold'})
plt.legend(loc='lower right')
plt.xlabel(r'$\alpha$', x=1, ha='right',size= 10 )
plt.ylabel('amplitude', y=1, ha='right',size= 10)
plt.show()

#...................GAMMA.............
fqy, fqx, ns= plt.hist(-gamma_test, bins=50, range=[min(-gamma_test), max(-gamma_test)], normed=True,fc='none', histtype='step', color='black', label='$\sigma_A-\sigma_B$', linewidth=.8)
plt.plot(-gamma_test_x, gamma_test_y, color='maroon', linewidth=.8, label='Fitting')
plt.text(min(-gamma_test_x) , max(fqy),'mean: '+str(round(-gamma_test_x.mean(),4))+'\n $\sigma$: '+str(round(gamma_test_x.std(),4))+'\n p: '+str(round(gamma_test_p, 4 )),{'fontsize': 10, 'weight' : 'bold'}, va="top", ha="left", bbox={'facecolor': 'white', 'alpha': 0.5, 'pad': 10})
plt.xticks([0],[0],size=12)
plt.yticks(size=10)
plt.legend(loc='upper right')
plt.fill_between(-gamma_test_x, gamma_test_y, where=-gamma_test_x<= 0,facecolor='maroon')
plt.xlabel('[1/m]', x=1, ha='right',size= 10 )
plt.ylabel('amplitude', y=1, ha='right',size= 10)
plt.show()

qx, by, n = plt.hist(gamma, bins=50, range=[min(gamma), max(gamma)], normed=True, fc='none', histtype='step', color='black', label='BTV', linewidth=.8)
plt.text(max(by) , max(qx),'mean: '+str(round(gamma.mean(),4))+'\n $\sigma$: '+str(round(gamma.std(),4)), va="top", ha="right", bbox={'facecolor': 'white', 'alpha': 0.5, 'pad': 10},color='black', fontdict={'weight' : 'bold', 'fontsize': 10})
qx1, by1, n1 = plt.hist(gamma1, bins=50, range=[min(gamma1), max(gamma1)], normed=True, fc='none', histtype='step', color = 'maroon', label='Basler', linewidth=.8)
plt.text(min(by1) , max(qx),'mean: '+str(round(gamma1.mean(),4))+'\n $\sigma$: '+str(round(gamma1.std(),4)) , va="top", ha="left", bbox={'facecolor': 'white', 'alpha': 0.5, 'pad': 10}, color='maroon', fontdict={'fontsize': 10,'weight' : 'bold'})
plt.legend(loc='lower right')
plt.xlabel(r'$\gamma$ [1/m]', x=1, ha='right',size= 10 )
plt.ylabel('amplitude', y=1, ha='right',size= 10)
plt.show()
