#-----------------Data Acquisition--------------------------#
#. This script extract beam images at the same time using   #
#. both Basler and BTV 620 cameras, using JAPC as main      #
#. resource.                                                #

import numpy as np
import matplotlib
matplotlib.use('Qt5Agg')
import pyjapc
japc = pyjapc.PyJapc("SCT.USER.ALL","CTF")
japc.rbacLogin()
import h5py
import time
import os

#..SETUP data.......
currents_start =5.0
currents_end   = 25.0
currents_step   = 1.0
currents = np.arange(currents_start,currents_end+currents_step*0.5,currents_step)

fprefix=input("File prefix = ")
quadrupole = input('Qua_scan_number:' )

if quadrupole == '510' or quadrupole == '520':
    Quad_x = "CA.QFD0510"
    Quad_y = "CA.QFD0520"
    Quad_x1 = "CA.QDD0515"
    camera= "CA.BTV0620"

elif quadrupole == '350' or quadrupole == '355':
    Quad_x = "CA.QFD0350"
    Quad_y = "CA.QDD0355"
    Quad_x1 = "CA.QFD0360"
    camera= "CA.BTV0390"
else:
    print('Error')

if quadrupole == '510' or quadrupole == '350':
    plane ='x'
elif quadrupole == '520' or quadrupole == '355':
    plane = 'y'
else:
    print('Error')

#New camera
camera_new= "CA.BTV0620-DIG"
camProperty_new = "Acquisition"
camField_new = "image1"
a=japc.getParam(camera_new + "/" + camProperty_new + "#" + camField_new)

#Old camera
#camera= "CA.BTV0620"
#camera= "CA.BTV0390"
camProperty = "Image"
camField = "imageSet"
imTest = japc.getParam(camera + "/" + camProperty)


def store_data(nameFile, nameData, data):
    hf= h5py.File(nameFile, 'w')
    hf.create_dataset(nameData, data=data)
    hf.close()
    return print('data saved')

def read_data(nameFile, nameData):
    hf = h5py.File(nameFile, 'r')
    data=hf[nameData][:]
    hf.close()
    return print(data)

def get_curr(Quad_name):
    current = japc.getParam(Quad_name + '/' + "Acquisition" + '#' + "currentAverage", timingSelectorOverride='SCT.USER.SETUP')
    return current


def set_curr(Quad_name, new_value):
    japc.setParam(Quad_name + '/' + "SettingPPM" + '#' + "current", new_value, timingSelectorOverride='')
    while True:
        print("wait for it...")
        time.sleep(1.0)
        nowCurrent = get_curr(Quad_name)
        if abs(nowCurrent-new_value) < 0.1:
            #Good enough!
            break;
        print("retry setting current")
        japc.setParam(Quad_name + '/' + "SettingPPM" + '#' + "current", new_value, timingSelectorOverride='')

    return get_curr(Quad_name)

hf = None

dataCounter = 0
isrunning = False
def myCallback( parameterName, camOldData ):
    global dataCounter, isrunning

    camNewData = japc.getParam(camera_new + "/" + camProperty_new + "#" + camField_new)
    camOldData=camOldData.reshape(len(imTest["imagePositionSet2"]), len(imTest["imagePositionSet1"]))

    hf.create_dataset("newCam_%05i"%(dataCounter,), data=camNewData)
    hf.create_dataset("oldCam_%05i"%(dataCounter,), data=camOldData)

    dataCounter += 1
    print(dataCounter)
    if dataCounter >= 10:
        #Reset
        japc.stopSubscriptions()
        isrunning=False
        dataCounter=0

print("Original currents:")
origCurrentx = get_curr(Quad_x)
origCurrenty = get_curr(Quad_y)
print("I"+ Quad_x, origCurrentx, "[A]")
print("I"+ Quad_y, origCurrenty, "[A]")

os.mkdir(os.getcwd()+'/'+fprefix)
#files=open("{}_qScan_{}.txt".format(fprefix,plane),'a')
files=open(os.getcwd()+'/'+fprefix+'/'+"{}_qScan_{}.txt".format(fprefix,plane),'a')
files.write(str(currents)+'\n')

for current in currents:
    if plane=='x':
        qname= Quad_x
        qname2= Quad_y
    else:
        qname=Quad_y
        qname2=Quad_x
    print("Setting current=",current,"[A]")
    set_curr(qname,current)

    nameFile = "{}_qScan_{}_{}.h5".format(fprefix,qname,current)
    print("nameFile =", nameFile)
    files.write(nameFile+'\n')

    hf = h5py.File(os.getcwd()+'/'+fprefix+'/'+nameFile, 'w')
    hf.attrs['I_'+qname] = current
    hf.attrs['I_'+qname2] = get_curr(qname2)
    hf.attrs['I_'+Quad_x1] = get_curr(Quad_x1)
    hf.attrs['Plane_'+plane] = plane

    japc.subscribeParam( camera+"/" + camProperty +"#" +camField, myCallback )
    isrunning = True
    japc.startSubscriptions()
    print("Getting data...")
    while True:
        time.sleep(1.0)
        if not isrunning:
            break
    print("Done")

    hf.close()
files.close()
print("reset to original current")
set_curr(Quad_x,origCurrentx)
set_curr(Quad_y,origCurrenty)
